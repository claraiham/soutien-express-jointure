const express = require("express");
const router = express.Router();

const connection = require("../conf/db");

router.get("/", (req, res) => {
  connection.query("SELECT * FROM game", (err, results) => {
    if (err) {
      res.status(500).send({
        message: "Erreur lors de la récupération des jeux",
        error: err,
      });
    } else {
      res.json(results);
    }
  });
});

// récupérer le jeu par son identifiant, avec aussi le nom de ses catégories

router.get('/:id/category', (req, res)=>{

  const id = req.params.id;

  connection.execute(`SELECT g.title, g.release_year, g.rate, c.name 
  FROM category c 
  LEFT JOIN game_category gc ON c.category_id = gc.category_id 
  LEFT JOIN game g ON gc.game_id =g.game_id 
  WHERE g.game_id = ? `,[id],(err, results)=>{
    if (err) {
      res.status(500).send({
        message: "Erreur lors de la récupération des jeux",
        error: err,
      });
    } else {
      res.json(results);
    }
  });

});

module.exports = router;
